//
//  BaseViewController.h
//  Murals Your Way
//
//  Created by Scott Mahonis on 8/10/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@end
