//
//  UIColor+Palatte.m
//
//  Created by Scott Mahonis on 5/23/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import "UIColor+Palatte.h"

@implementation UIColor (Palatte)

+(UIColor *)mainColor {
    return [UIColor colorWithRed:4./255. green:202./255. blue:208./255. alpha:1.0];
}

+(UIColor *)secondColor {
    return [UIColor colorWithRed:245./255. green:120./255. blue:134./255. alpha:1.0];
}

+(UIColor *)randomColor {
    NSInteger aRedValue = arc4random()%255;
    NSInteger aGreenValue = arc4random()%255;
    NSInteger aBlueValue = arc4random()%255;
    
    UIColor *randColor = [UIColor colorWithRed:aRedValue/255.0f green:aGreenValue/255.0f blue:aBlueValue/255.0f alpha:1.0f];
    if ([UIColor isTooCloseToBeingWhite:randColor]) {
        return [UIColor randomColor];
    } else {
        return randColor;
    }
}

+(BOOL)isTooCloseToBeingWhite:(UIColor *)color {
    BOOL isWhite = NO;
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    float red = components[0];
    float green = components[1];
    float blue = components[2];
    float avg = (red + green + blue)/3;
    if (avg > 0.6) {
        isWhite = YES;
    }
    return isWhite;
}

+(BOOL)isMoreWhiteThanBlack:(UIColor *)color {
    BOOL isWhite = NO;
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    float red = components[0];
    float green = components[1];
    float blue = components[2];
    float avg = (red + green + blue)/3;
    if (avg > 0.5) {
        isWhite = YES;
    }
    return isWhite;
}

@end
