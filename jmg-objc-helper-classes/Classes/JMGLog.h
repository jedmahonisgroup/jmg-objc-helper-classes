//
//  JMGLog.h
//  Green Mill
//
//  Created by Scott Mahonis on 10/12/17.
//

#import <UIKit/UIKit.h>

@interface JMGLog : NSObject

+(void)jmgLogWithFormat:(NSString *)format andShouldLogAnError:(BOOL)shouldLogError;

@end
