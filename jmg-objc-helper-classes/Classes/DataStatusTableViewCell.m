//
//  DataStatusTableViewCell.m
//  Kuikstaff
//
//  Created by Scott Mahonis on 1/17/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import "DataStatusTableViewCell.h"

@implementation DataStatusTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setUIToShowErrorWithErrorString:(NSString *)errorString {
    [self.activityIndicator stopAnimating];
    self.subLabel.text = @"Touch to retry";
    self.mainLabel.text = errorString;
    self.dataStatusImageView.image = [UIImage imageNamed:@"error"];
}

-(void)setUIToShowLoadingWithText:(NSString *)text {
    [self.activityIndicator startAnimating];
    self.subLabel.text = @"";
    self.mainLabel.text = text ? text : @"Loading";
    self.dataStatusImageView.image = nil;
}

-(void)setUIToShowNoObjectsFoundWithImageNamed:(NSString *)imageName andMainLabelText:(NSString *)mainLabelText andSubLabelText:(NSString *)subLabelText {
    NSLog(@"here");
    [self.activityIndicator stopAnimating];
    NSLog(@" not here");
    self.subLabel.text = subLabelText;
    self.mainLabel.text = mainLabelText;
    self.dataStatusImageView.image = [UIImage imageNamed:imageName];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
