//
//  User.h
//  Triangle
//
//  Created by Scott Mahonis on 10/9/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject <NSCoding>

@property (nonatomic) NSString *userID;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;


-(void)save;
+(User *)currentUser;


@end
