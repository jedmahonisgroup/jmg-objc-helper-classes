//
//  DataStatusTableViewCell.h
//  Kuikstaff
//
//  Created by Scott Mahonis on 1/17/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataStatusTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *dataStatusImageView;
@property (nonatomic, weak) IBOutlet UILabel *mainLabel;
@property (nonatomic, weak) IBOutlet UILabel *subLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

-(void)setUIToShowErrorWithErrorString:(NSString *)errorString;
-(void)setUIToShowLoadingWithText:(NSString *)text;
-(void)setUIToShowNoObjectsFoundWithImageNamed:(NSString *)imageName andMainLabelText:(NSString *)mainLabelText andSubLabelText:(NSString *)subLabelText;

@end
