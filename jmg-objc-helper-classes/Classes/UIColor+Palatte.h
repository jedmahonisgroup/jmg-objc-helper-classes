//
//  UIColor+Palette.h
//
//  Created by Scott Mahonis on 5/23/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Palatte)

+(UIColor *)mainColor;

+(UIColor *)secondColor;

+(UIColor *)randomColor;

+(BOOL)isMoreWhiteThanBlack:(UIColor *)color;

@end
