//
//  LoadingView.h
//  Companies Like Me
//
//  Created by Scott Mahonis on 9/14/15.
//  Copyright (c) 2015 Tim Bornholdt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

-(instancetype)initWithFrame:(CGRect)frame andText:(NSString *)text;

- (void) showActivityIndicator;

- (void) hideActivityIndicator;

+(LoadingView *)addLoadingViewWithText:(NSString *)text;

+(LoadingView *)defaultLoadingView;

@end
