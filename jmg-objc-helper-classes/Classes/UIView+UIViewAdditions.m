//
//  UIView+UIViewAdditions.m
//  Kuikstaff
//
//  Created by Scott Mahonis on 4/17/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import "UIView+UIViewAdditions.h"

@implementation UIView (UIViewAdditions)

@dynamic borderColor,borderWidth,cornerRadius;

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    [self.layer setCornerRadius:cornerRadius];
}

@end
