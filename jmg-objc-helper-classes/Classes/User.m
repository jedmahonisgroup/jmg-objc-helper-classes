//
//  User.m
//  Triangle
//
//  Created by Scott Mahonis on 10/9/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import "User.h"
#define kUserID @"userID"
#define kFirstName @"firstName"
#define kLastName @"lastName"

@implementation User

-(NSString *)description {
    return [NSString stringWithFormat:@"{\r    UserID:%@\r    FirstName:%@\r    lastName:%@\r}", self.userID, self.firstName, self.lastName];
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super init]) {
        self.userID = [aDecoder decodeObjectForKey:kUserID];
        self.firstName = [aDecoder decodeObjectForKey:kFirstName];
        self.lastName = [aDecoder decodeObjectForKey:kLastName];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:self.userID forKey:kUserID];
    [aCoder encodeObject:self.firstName forKey:kFirstName];
    [aCoder encodeObject:self.lastName forKey:kLastName];
}

-(void)save {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"currentUser"];
    [NSKeyedArchiver archiveRootObject:self toFile:appFile];
}

+(User *)currentUser {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:@"currentUser"];
    User *currentUser = [NSKeyedUnarchiver unarchiveObjectWithFile:appFile];
    return currentUser;
}



@end
