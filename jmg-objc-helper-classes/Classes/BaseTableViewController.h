//
//  BaseTableViewController.h
//  Kuikstaff
//
//  Created by Scott Mahonis on 1/17/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface BaseTableViewController : UITableViewController <UITextViewDelegate>

@property (nonatomic) BOOL isLoading;
@property (nonatomic, strong) NSString *errorString;
@property (nonatomic) IBOutlet UIToolbar *keyboardToolbar;
@property (nonatomic) User *user;


-(IBAction)exit:(id)sender;



@end
