//
//  SessionManager.m
//  Kuikstaff
//
//  Created by Scott Mahonis on 1/10/17.
//  Copyright © 2017 JMG. All rights reserved.
//

//#import "SessionManager.h"

//@implementation SessionManager

//+(AFHTTPSessionManager *)jsonManager {
//    static SessionManager *manager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        manager = (SessionManager *)[AFHTTPSessionManager manager];
//    });
//    AFHTTPRequestSerializer *reqSerializer = [AFJSONRequestSerializer serializer];
//    [reqSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    reqSerializer.timeoutInterval = 60.;
//
//    manager.requestSerializer = reqSerializer;
//    return manager;
//}
//
//+(AFHTTPSessionManager *)httpManager {
//    static SessionManager *manager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        manager = (SessionManager *)[AFHTTPSessionManager manager];
//    });
//    AFHTTPRequestSerializer *reqSerializer = [AFHTTPRequestSerializer serializer];
//    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
//    reqSerializer.timeoutInterval = 60.;
//    
//    manager.requestSerializer = reqSerializer;
//    return manager;
//}

//+(void)refreshWithSuccess:(void (^)(id responseObject))success failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {
//    AFOAuthCredential *cred = [AFOAuthCredential retrieveCredentialWithIdentifier:@"access_token"];
//    AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    NSLog(@"cred.refreshToken:%@", cred.refreshToken);
//    [[SessionManager manager] POST:[NSString stringWithFormat:@"%@/oauth/token", del.kAPIURL] parameters:@{@"grant_type":@"refresh_token", @"refresh_token":cred.refreshToken} progress:^(NSProgress * _Nonnull uploadProgress) {
//        NULL;
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSLog(@"refresh called successfully :%@", responseObject);
//        [User setStoredToken:responseObject[@"access_token"] andRefreshToken:responseObject[@"refresh_token"] withExpires:[NSDate dateWithTimeIntervalSinceNow:[responseObject[@"expires_in"] intValue]]];
//        //TODO: Store in keychain later
//        success(responseObject);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        AppDelegate *del = (AppDelegate *)[UIApplication sharedApplication].delegate;
//        NSLog(@"error refreshWithSuccess:%@ %@", [NSString stringWithFormat:@"%@/oath/token", del.kAPIURL], error.localizedDescription);
//        failure(task, error);
//    }];
//}

//@end

