//
//  JMGLog.m
//  Green Mill
//
//  Created by Scott Mahonis on 10/12/17.
//

#import "JMGLog.h"
#import <Crashlytics/Crashlytics.h>

@implementation JMGLog

+(void)jmgLogWithFormat:(NSString *)format andShouldLogAnError:(BOOL)shouldLogError {
    if (kDebug) {
        NSLog(@"%@", format);
    } else if (shouldLogError) {
        if (CrashlyticsKit) {
            CLSLog(@"server fail:%@", format);
            [Answers logCustomEventWithName:@"ServerFail" customAttributes:@{@"error_description":format}];
        }
    }
}

@end
