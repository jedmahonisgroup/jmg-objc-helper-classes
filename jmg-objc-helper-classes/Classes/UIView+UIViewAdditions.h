//
//  UIView+UIViewAdditions.h
//  Kuikstaff
//
//  Created by Scott Mahonis on 4/17/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIViewAdditions)

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;

@end
