//
//  KwiklyNavigationController.h
//  Kuikstaff
//
//  Created by Scott Mahonis on 4/17/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController

@property (nonatomic, strong) UIColor *randomColor;

-(void)setNavBarToDefault;
-(void)setNavBarToClearMode;
-(void)lightItUpWithColor;

@end
