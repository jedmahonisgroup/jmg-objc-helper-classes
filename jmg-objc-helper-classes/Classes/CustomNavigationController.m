//
//  CustomNavigationController.m
//
//  Created by Scott Mahonis on 4/17/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import "CustomNavigationController.h"
#import "UIColor+Palatte.h"

@interface CustomNavigationController ()

@end

@implementation CustomNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBar.translucent = NO;
    [self lightItUpWithColor];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
}

-(UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, self.navigationBar.frame.size.width, [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationBar.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)setNavBarToDefault {
    //[self imageFromColor:[UIColor kwiklyBlue]]
    _randomColor = [UIColor clearColor];
    
    [self.navigationBar setBackgroundImage:[self imageFromColor:_randomColor] forBarMetrics:UIBarMetricsDefault];
//    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont systemFontOfSize:18. weight:UIFontWeightRegular]};
//    self.navigationBar.tintColor = [UIColor isMoreWhiteThanBlack:randomColor] ? [UIColor darkGrayColor] : [UIColor whiteColor];
    self.navigationBar.barStyle = UIBarStyleBlack;
    [NSNotificationCenter.defaultCenter postNotificationName:@"colorChange" object:nil userInfo:@{@"color":_randomColor}];
    self.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)lightItUpWithColor {
    if (_randomColor) {
        return;
    }
    _randomColor = [UIColor randomColor];
    self.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationBar setBackgroundImage:[self imageFromColor:_randomColor] forBarMetrics:UIBarMetricsDefault];
    //    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor], NSFontAttributeName:[UIFont systemFontOfSize:18. weight:UIFontWeightRegular]};
//    self.navigationBar.tintColor = [UIColor isMoreWhiteThanBlack:randomColor] ? [UIColor darkGrayColor] : [UIColor whiteColor];
    self.navigationBar.barStyle = UIBarStyleBlack;
//    [NSNotificationCenter.defaultCenter postNotificationName:@"colorChange" object:nil userInfo:@{@"color":randomColor}];
}

-(void)setNavBarToClearMode {
    [self.navigationBar setBackgroundImage:[self imageFromColor:[UIColor clearColor]] forBarMetrics:UIBarMetricsDefault];
    self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.navigationBar.tintColor = [UIColor mainColor];
    self.navigationBar.barStyle = UIBarStyleDefault;
    [self.navigationBar setShadowImage:[UIImage new]];
}

-(UIImage *)gradientBlueImage {
    
    const CGFloat WIDTH = self.navigationBar.frame.size.width;
    const CGFloat HEIGHT = [UIApplication sharedApplication].statusBarFrame.size.height + self.navigationBar.frame.size.height;
    
    //  Do we want our UIImage to fade from black-to-red or black-to-blue ?
    UIColor* color1 = [self colorFromHexString:@"3f5866"];
    UIColor* color2 = [self colorFromHexString:@"455975"];
    UIColor* color3 = [self colorFromHexString:@"495e7a"];
    UIColor* color4 = [self colorFromHexString:@"6f84a2"];
    
    CGSize size = CGSizeMake(WIDTH, HEIGHT);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    
    const CGFloat *components1 = CGColorGetComponents(color1.CGColor);
    CGFloat red1 = components1[0];
    CGFloat green1 = components1[1];
    CGFloat blue1 = components1[2];
    
    const CGFloat *components2 = CGColorGetComponents(color2.CGColor);
    CGFloat red2 = components2[0];
    CGFloat green2 = components2[1];
    CGFloat blue2 = components2[2];
    
    const CGFloat *components3 = CGColorGetComponents(color3.CGColor);
    CGFloat red3 = components3[0];
    CGFloat green3 = components3[1];
    CGFloat blue3 = components3[2];
    
    const CGFloat *components4 = CGColorGetComponents(color4.CGColor);
    CGFloat red4 = components4[0];
    CGFloat green4 = components4[1];
    CGFloat blue4 = components4[2];
    
    size_t gradientNumberOfLocations = 4;
    CGFloat gradientLocations[4] = { 0.0, 0.3333, 0.6666, 1.0 };
    CGFloat gradientComponents[16] = { red1, green1, blue1, 1.0,
        red2, green2, blue2, 1.0,
        red3, green3, blue3, 1.0,
        red4, green4, blue4, 1.0 };
    
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents (colorspace, gradientComponents, gradientLocations, gradientNumberOfLocations);
    
    //  Create a UIImage containing this gradient
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //  Make sure the gradient is horizontal
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(WIDTH, 0), 0);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorspace);
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIColor *) colorFromHexString:(NSString *)hexString{
    NSString *cleanString = [hexString stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],[cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],[cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],[cleanString substringWithRange:NSMakeRange(2, 1)]];
    }
    if([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }
    
    unsigned int baseValue;
    [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];
    
    float red = ((baseValue >> 24) & 0xFF)/255.0f;
    float green = ((baseValue >> 16) & 0xFF)/255.0f;
    float blue = ((baseValue >> 8) & 0xFF)/255.0f;
    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

@end
