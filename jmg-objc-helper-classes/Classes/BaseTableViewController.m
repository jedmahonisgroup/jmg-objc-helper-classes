//
//  BaseTableViewController.m
//  Kuikstaff
//
//  Created by Scott Mahonis on 1/17/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#import "BaseTableViewController.h"
#import "CustomNavigationController.h"

@interface BaseTableViewController ()

@end

@implementation BaseTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.user = User.currentUser;
    [self.tableView registerNib:[UINib nibWithNibName:@"DataStatusTableViewCell" bundle:nil] forCellReuseIdentifier:@"DataStatusTableViewCell"];
//    [[NSBundle mainBundle] loadNibNamed:@"KeyboardToolbar" owner:self options:nil];
    NSArray *bundle = [[NSBundle mainBundle] loadNibNamed:@"KeyboardToolbar" owner:self options:nil];
    for (id object in bundle) {
        if ([object isKindOfClass:[UIToolbar class]]) {
            _keyboardToolbar = (UIToolbar *)object;
            break;
        }
    }
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    
}

-(IBAction)exit:(id)sender {
    if (self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

//-(UIToolbar *)keyboardToolbar {
//    NSArray *bundle = [[NSBundle mainBundle] loadNibNamed:@"KeyboardToolbar" owner:self options:nil];
//    for (id object in bundle) {
//        if ([subView isKindOfClass:[UIToolbar class]]) {
//            _keyboardToolbar = (UIToolbar *)subView;
//            break;
//        }
//    }
//    return _keyboardToolbar;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:((CustomNavigationController*)self.navigationController).randomColor];
    header.contentView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    header.textLabel.font = [UIFont systemFontOfSize:16. weight:UIFontWeightBold];
    
}

#pragma mark - UITextField Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.keyboardToolbar.tintColor = ((CustomNavigationController*)self.navigationController).randomColor;
    textField.inputAccessoryView = self.keyboardToolbar;
    for (UIBarButtonItem *item in self.keyboardToolbar.items) {
        item.tag = textField.tag;
    }
    
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    self.keyboardToolbar.tintColor = ((CustomNavigationController*)self.navigationController).randomColor;
    textView.inputAccessoryView = _keyboardToolbar;
    for (UIBarButtonItem *item in _keyboardToolbar.items) {
        item.tag = textView.tag;
    }
    return YES;
}

-(IBAction)nextTextField:(UIBarButtonItem *)sender {
    long tag = sender.tag;
    id nextView = [self.view viewWithTag:tag + 1];
    if (nextView && [nextView respondsToSelector:@selector(becomeFirstResponder)]) {
        [nextView becomeFirstResponder];
    } else {
        [self dismissKeyboard:sender];
    }
}

-(IBAction)prevTextField:(UIBarButtonItem *)sender {
    long tag = sender.tag;
    id previousView = [self.view viewWithTag:tag - 1];
    if (previousView && [previousView respondsToSelector:@selector(becomeFirstResponder)]) {
        [previousView becomeFirstResponder];
    } else {
        [self dismissKeyboard:sender];
    }
}

-(IBAction)dismissKeyboard:(UIBarButtonItem *)sender {
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    UIBarButtonItem *item = [UIBarButtonItem new];
    item.tag = textField.tag;
    [self nextTextField:item];
    return YES;
}

@end
