//
//  Constants.h
//  Murals Your Way
//
//  Created by Scott Mahonis on 8/9/17.
//  Copyright © 2017 JMG. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define kAPIURL @"https://www.muralsmyway.com/rest"
#ifdef DEBUG
#define kDebug YES
#else
#define kDebug NO
#endif
#define kDatabaseName @"triangleDatabase.db"
//#define kTargetFileSize CGSizeMake(150.0f, 150.0f)

#endif /* Constants_h */
