#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double jmg_objc_helper_classesVersionNumber;
FOUNDATION_EXPORT const unsigned char jmg_objc_helper_classesVersionString[];

