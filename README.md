# jmg-objc-helper-classes

[![CI Status](http://img.shields.io/travis/scottmahonis/jmg-objc-helper-classes.svg?style=flat)](https://travis-ci.org/scottmahonis/jmg-objc-helper-classes)
[![Version](https://img.shields.io/cocoapods/v/jmg-objc-helper-classes.svg?style=flat)](http://cocoapods.org/pods/jmg-objc-helper-classes)
[![License](https://img.shields.io/cocoapods/l/jmg-objc-helper-classes.svg?style=flat)](http://cocoapods.org/pods/jmg-objc-helper-classes)
[![Platform](https://img.shields.io/cocoapods/p/jmg-objc-helper-classes.svg?style=flat)](http://cocoapods.org/pods/jmg-objc-helper-classes)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

jmg-objc-helper-classes is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'jmg-objc-helper-classes'
```

## Author

scottmahonis, rob@jedmahonisgroup.com

## License

jmg-objc-helper-classes is available under the MIT license. See the LICENSE file for more info.
