#
# Be sure to run `pod lib lint jmg-objc-helper-classes.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'jmg-objc-helper-classes'
  s.version          = '1.0.12'
  s.summary          = 'These files are useful for initial project set up and speed up development'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = 'To Include all these files in your project, add a Prefix.pch and add "#import Include.h" to it.'

  s.homepage         = 'https://bitbucket.org/jedmahonisgroup/jmg-objc-helper-classes'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'scottmahonis' => 'rob@jedmahonisgroup.com' }
  s.source           = { :git => 'https://bitbucket.org/jedmahonisgroup/jmg-objc-helper-classes.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

s.source_files = 'jmg-objc-helper-classes/Classes/*'

   s.resource_bundles = {
       'jmg-objc-helper-classes' => ['jmg-objc-helper-classes/Assets/*.{png, xib}']
   }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end

# https://bitbucket.org/jedmahonisgroup/jmg-objc-helper-classes.git
